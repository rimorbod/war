import java.util.*;

public class War {
    public static void main(String[] args) {

        List<Card> talia = new ArrayList();
        fill(talia);
        shuffle(talia);
        Queue<Card> p1 = new LinkedList<>();
        Queue <Card> p2 = new LinkedList<>();
        distribute(talia, p1, p2);
        while(p1.size()>0&&p2.size()>0) {
            fight(p1, p2);
        }
        if (p1.size()>p2.size()){
            System.out.println("Gratulacje, wygrałeś!");
        }else{
            System.out.println("Spróbuj ponownie, tym razem wygrał komputer :(");
        }

    }

    private static void fight(Queue<Card> p1, Queue<Card> p2) {

        Queue<Card> table = new LinkedList<>();
        Card player = p1.poll();
        Card computer = p2.poll();
        System.out.println("Ty: " + player.getName() + " Komputer: " + computer.getName());
        System.out.println("---------------------------------------------------------");
        table.offer(player);
        table.offer(computer);
        while(player.getValue()==computer.getValue()){
            System.out.println("WAR!");
            player = p1.poll();
            computer = p2.poll();
            System.out.println("Ty: " + player.getName() + " Komputer: " + computer.getName());
            System.out.println("---------------------------------------------------------");
            table.offer(player);
            table.offer(computer);
        }
        if (player.getValue()>computer.getValue()){
            for (Card card : table) {
                p1.offer(card);
            }
        }else if (player.getValue()<computer.getValue()){
            for (Card card : table) {
                p2.offer(card);
            }
        }
    }

    private static void shuffle(List<Card> talia) {
        Random random = new Random();
        Card shuffle [] =new Card[talia.size()];
        for (int i=0; i<talia.size(); i++){
            int a = random.nextInt(52);
            while (shuffle[a]!=null){
                a = random.nextInt(52);
            }
            shuffle[a]=talia.get(i);
        }
        talia.clear();
        for (Card card : shuffle) {
            talia.add(card);
        }
    }

    private static void distribute(List<Card> talia, Queue<Card> p1, Queue<Card> p2) {
        for (int i=0; i<talia.size(); i++){
            if (i%2!=0){
                p1.offer(talia.get(i));
            }else{
                p2.offer(talia.get(i));
            }
        }
    }

    private static void fill(List<Card> talia) {
        talia.add(new Card("2♥", 2));
        talia.add(new Card("3♥", 3));
        talia.add(new Card("4♥", 4));
        talia.add(new Card("5♥", 5));
        talia.add(new Card("6♥", 6));
        talia.add(new Card("7♥", 7));
        talia.add(new Card("8♥", 8));
        talia.add(new Card("9♥", 9));
        talia.add(new Card("10♥",10));
        talia.add(new Card("J♥", 11));
        talia.add(new Card("Q♥", 12));
        talia.add(new Card("K♥", 13));
        talia.add(new Card("A♥", 14));
        talia.add(new Card("2♦", 2));
        talia.add(new Card("3♦", 3));
        talia.add(new Card("4♦", 4));
        talia.add(new Card("5♦", 5));
        talia.add(new Card("6♦", 6));
        talia.add(new Card("7♦", 7));
        talia.add(new Card("8♦", 8));
        talia.add(new Card("9♦", 9));
        talia.add(new Card("10♦",10));
        talia.add(new Card("J♦", 11));
        talia.add(new Card("Q♦", 12));
        talia.add(new Card("K♦", 13));
        talia.add(new Card("A♦", 14));
        talia.add(new Card("2♣", 2));
        talia.add(new Card("3♣", 3));
        talia.add(new Card("4♣", 4));
        talia.add(new Card("5♣", 5));
        talia.add(new Card("6♣", 6));
        talia.add(new Card("7♣", 7));
        talia.add(new Card("8♣", 8));
        talia.add(new Card("9♣", 9));
        talia.add(new Card("10♣",10));
        talia.add(new Card("J♣", 11));
        talia.add(new Card("Q♣", 12));
        talia.add(new Card("K♣", 13));
        talia.add(new Card("A♣", 14));
        talia.add(new Card("2♠", 2));
        talia.add(new Card("3♠", 3));
        talia.add(new Card("4♠", 4));
        talia.add(new Card("5♠", 5));
        talia.add(new Card("6♠", 6));
        talia.add(new Card("7♠", 7));
        talia.add(new Card("8♠", 8));
        talia.add(new Card("9♠", 9));
        talia.add(new Card("10♠",10));
        talia.add(new Card("J♠", 11));
        talia.add(new Card("Q♠", 12));
        talia.add(new Card("K♠", 13));
        talia.add(new Card("A♠", 14));
    }
}

